# My Dotfiles

For Debian and Ubuntu.

## Window Manager and Tools

Run `sudo ./install-dwm.sh` to install the following:

### Window Manager

* [dwm (fork)](https://bitbucket.org/christiankuenne/dwm/) (Window Manager)
* [XDM](https://en.wikipedia.org/wiki/XDM_(display_manager)) (Login Manager)
* [st (fork)](https://bitbucket.org/christiankuenne/st/) (Terminal)
* [Rofi](https://github.com/davatorium/rofi) (Application Launcher)
* [Plano Theme](https://github.com/lassekongo83/plano-theme) (GTK Theme)
* [Papirus](https://github.com/PapirusDevelopmentTeam/papirus-icon-theme) (Icon Theme)

### Tools

* [ConnMan](https://wiki.archlinux.org/index.php/ConnMan) (Network Manager)
* [Firefox ESR](https://www.mozilla.org/en-US/firefox/organizations/) (Browser)
* [Galculator](http://galculator.mnim.org/) (Calculator)
* [GPicView](https://wiki.lxde.org/en/GPicView) (Image Viewer)
* [htop](https://hisham.hm/htop/) (System Monitor)
* [i3lock](https://i3wm.org/i3lock/) (Screen Locker)
* [LXAppearance](https://wiki.lxde.org/en/LXAppearance) (Look and Feel)
* [LXPolkit](https://packages.debian.org/stretch/lxpolkit) (Authentication)
* [LXRandR](https://wiki.lxde.org/en/LXRandR) (Screen Settings)
* [Mousepad](https://github.com/codebrainz/mousepad) (Text Editor)
* [mtPaint](http://mtpaint.sourceforge.net/) (Image Editor)
* [Nitrogen](https://wiki.archlinux.org/index.php/Nitrogen) (Wallpaper Setter)
* [PulseAudio](https://wiki.archlinux.org/index.php/PulseAudio) (Volume Control)
* [Scrot](https://en.wikipedia.org/wiki/Scrot) (Screenshot Utility)
* [Thunar](https://docs.xfce.org/xfce/thunar/start) (File Manager)

## Applications

Run `sudo ./install-apps.sh` to install:

* [KeePassXC](https://keepassxc.org/) (Password Manager)
* [LibreOffice](https://www.libreoffice.org/) (Office Suite)
* [Meld](https://meldmerge.org/) (Diff Tool)
* [VLC Player](https://www.videolan.org/vlc/index.html) (Media Player)
* [youtube-dl](https://ytdl-org.github.io/youtube-dl/index.html) (Media Downloader)

## Dev Tools

Run `sudo ./install-dev.sh` to install:

* [.NET Core SDK](https://dotnet.microsoft.com/download)
* [Docker](https://www.docker.com/)
* [JDK](https://wiki.debian.org/Java)
* [Node.js](https://nodejs.org/en/)
* [Visual Studio Code](https://code.visualstudio.com/)