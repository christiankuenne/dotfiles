#!/bin/bash

# Check if su
if [ "$SUDO_USER" = '' ]
then
  echo 'Please run as super user'
  exit 1
fi

# Dev setup
sudo -u $SUDO_USER git clone https://bitbucket.org/christiankuenne/devsetup.git temp/devsetup
cd temp/devsetup
./dev.sh
cd ../..

# Add Visual Studio Code repo
sudo -u $SUDO_USER curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > packages.microsoft.gpg
install -o root -g root -m 644 packages.microsoft.gpg /usr/share/keyrings/
rm -f packages.microsoft.gpg
sh -c 'echo "deb [arch=amd64 signed-by=/usr/share/keyrings/packages.microsoft.gpg] https://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list'

# Install Visual Studio Code
apt update
apt -y install code
