#!/bin/bash

# Check if su
if [ "$SUDO_USER" = '' ]
then
  echo 'Please run as super user'
  exit 1
fi

# Variables
SUDO_HOME="/home/$SUDO_USER"

# KeePassXC
apt install -y keepassxc

# Meld Diff
apt install -y meld

# VLC Player
apt install -y vlc

# YouTube DL
curl -L https://yt-dl.org/downloads/latest/youtube-dl -o /usr/local/bin/youtube-dl
chmod a+rx /usr/local/bin/youtube-dl
