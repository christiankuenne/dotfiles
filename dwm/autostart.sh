#!/bin/bash

# Startup
nitrogen --restore &
# Set Keyboard Layout (necessary for xdotool)
setxkbmap
# Lock screen after 10 minutes (unless the cursor is in the top left or top right corner)
xautolock -time 10 -locker 'i3lock -c "#000000"' -corners --00 &

# Status Bar

# Prepare Battery Check
battery_path='/sys/class/power_supply/BAT0'
if [ ! -d $battery_path ]
then
	battery_path=''
fi

get_battery() {
	# Return if no Battery found
	if [ "$battery_path" == '' ]
	then
		return 0
	fi
	# Get Battery Status
	declare -i percent=`cat $battery_path/capacity`
	status=`cat $battery_path/status`
	# Status Message
	if [ $status == "Charging" ]
	then
		echo " $percent%   "
	else
		if (( $percent >= 90 ))
		then
			echo " $percent%   "
		elif (( $percent >= 66 ))
		then
			echo " $percent%   "
		elif (( $percent >= 33 ))
		then
			echo " $percent%   "
		elif (( $percent >= 10 ))
		then
			echo " $percent%   "
		else
			echo " $percent%   "
		fi
	fi
}

get_volume() {
	declare -i vol=`pactl list sinks | grep '^[[:space:]]Volume:' | head -n $(( $SINK + 1 )) | tail -n 1 | sed -e 's,.* \([0-9][0-9]*\)%.*,\1,'`
	if (( $vol > 0 ))
	then
		echo '   '
	fi
}

get_bluetooth() {
	if [[ $(hciconfig | sed '3!d') == *"UP"* ]]
	then
		echo '   '
	fi
}

get_clock() {
	echo " $(date +'%a %d %b %H:%M')"
}

get_network() {
	interface=`ip addr | awk '/state UP/ {print $2}'`
	if [[ $interface == "w"* ]]
	then
		echo "   "
	elif [[ $interface == "e"* ]]
	then
		echo "   "
	fi
}

# Check if dwm is still running
is_wm_running() {
	str=`wmctrl -m | grep Name | cut -d ' ' -f 2`
	if [ "$str" == 'dwm' ]
	then
		echo 0
	fi
}

# Main loop
while [ $(is_wm_running) ]
do
	# Output
	xsetroot -name "$(get_volume)$(get_bluetooth)$(get_network)$(get_battery)$(get_clock)  "
	# Sleep 5 seconds
	sleep 5;
done
