#!/bin/bash

# Settings
password_file="$HOME/passwords.kdbx"
keyring_name='keepass'
keyring_value=$USER
group='Internet/'

# Run this to add the master password to the keyring
# secret-tool store --label="Keepass $USER" keepass $USER

# Get master password from keyring
master_pw=`secret-tool lookup $keyring_name $keyring_value`
if [ "$master_pw" == '' ]
then
	exit 1
fi

# Select entry
# $1: group
select_entry() {
	# Add option navigate back to root (if in group)
	if [ "$1" == '' ]
	then
		back=''
	else
		back=".\n"
	fi
	entries=`echo $master_pw | keepassxc-cli ls "$password_file" "$1" | tail -n +2 | sort`;
	echo -e "$back$entries" | rofi -dmenu -i -p 'My Passwords';
}

# Get details
# $1: group
# $2: entry
get_details() {
	echo $master_pw | keepassxc-cli show "$password_file" "$1$2"
}

# Get value
# $1: string
# $2: key
get_value() {
	echo $1 | sed "s/.*$2: //" | sed 's/\s.*//'
}

# Main loop
while [ true ]
do
	entry=$(select_entry "$group")
	if [ "$entry" == '' ]
	then
		#User exit
		exit 0
	elif [ "$entry" == '.' ]
	then
		# Back to root
		group=$(select_entry)
		echo $group
	elif [[ "$entry" == *"/" ]]
	then
		# Open group
		group=$group$entry
	else
		# Get entry
		str=$(get_details "$group""$entry");
		user_name=$(get_value "$str" UserName);
		pw=$(get_value "$str" Password);
		url=$(get_value "$str" URL);
		# User and Password into Clipboard
		echo "$user_name|$pw" | xclip -selection clipboard
		# Open Browser
		xdg-open "$url"
		exit 0;
	fi
done