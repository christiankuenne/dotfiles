#!/bin/bash

# Check if su
if [ "$SUDO_USER" = '' ]
then
  echo 'Please run as super user'
  exit 1
fi

# Variables
SUDO_HOME="/home/$SUDO_USER"

# Debian or Ubuntu?
os=$(lsb_release -is)

# Config folder
echo "Configure $os"
sudo -u $SUDO_USER mkdir -p $SUDO_HOME/.config
sudo -u $SUDO_USER mkdir -p $SUDO_HOME/.dwm

# X11
apt install -y xorg xserver-xorg-video-intel

# Required Software
echo "Installing required software"
apt install -y at-spi2-core curl dbus-user-session dbus-x11 dunst fonts-font-awesome gnome-keyring gridsite-clients gsfonts-x11 gvfs-backends i3lock libexo-1-0 libnotify-bin libsecret-tools locate python python-xdg python3-xdg ssh tumbler upower xautolock xclip xdg-utils xdm xdotool xsettingsd

# Applications
echo "Install Applications"
apt install -y connman galculator gpicview grabc htop lxappearance lxpolkit lxrandr mousepad mtpaint nitrogen pavucontrol pulseaudio rofi scrot slop thunar --no-install-recommends thunar-archive-plugin --no-install-recommends unzip xarchiver

# Firefox
if [ "$os" == 'Debian' ]
then
	apt install -y firefox-esr
else
	apt install -y firefox
fi

# Install dwm
apt install -y wmctrl
apt install -y build-essential libx11-dev libxft-dev libxinerama-dev
sudo -u $SUDO_USER git clone https://bitbucket.org/christiankuenne/dwm.git temp/dwm
make install -C temp/dwm

# Install st
sudo -u $SUDO_USER git clone https://bitbucket.org/christiankuenne/st.git temp/st
make install -C temp/st
# Make st available as default Terminal
update-alternatives --install /usr/bin/x-terminal-emulator x-terminal-emulator /usr/local/bin/st 30

# Install Bookmark Manager
sudo -u $SUDO_USER git clone https://bitbucket.org/christiankuenne/eselsohr.git temp/eselsohr
make install -C temp/eselsohr

# GTK Themes
echo "Install GTK Engines"
apt install -y gtk2-engines-murrine --no-install-recommends gtk2-engines-pixbuf

echo "Download Equilux Theme (Dark Theme)"
sudo -u $SUDO_USER git clone https://github.com/ddnexus/equilux-theme temp/equilux-theme
cd temp/equilux-theme
./install.sh
cd ../..

echo "Download Plano Theme (Light Theme)"
sudo -u $SUDO_USER git clone https://github.com/lassekongo83/plano-theme temp/plano-theme
cp -rf temp/plano-theme /usr/share/themes

# Icon Theme
echo "Papirus Icon Theme"
apt install -y papirus-icon-theme

# Config Files
echo "Configure dwm"
sudo -u $SUDO_USER cp -rf dwm/autostart.sh $SUDO_HOME/.dwm

echo "Configure Rofi"
sudo -u $SUDO_USER mkdir -p $SUDO_HOME/.config/rofi
sudo -u $SUDO_USER cp -rf rofi/config $SUDO_HOME/.config/rofi
cp -rf rofi/plano.rasi /usr/share/rofi/themes
cp -rf rofi/tralee.rasi /usr/share/rofi/themes
cp -rf rofi/tralee-dark.rasi /usr/share/rofi/themes

echo "Configure Thunar"
sudo -u $SUDO_USER cp -rf Thunar $SUDO_HOME/.config

echo "Configure GtkSourceView (Syntax Highlighting in Mousepad Editor)"
mkdir -p /usr/share/gtksourceview-3.0/styles
cp -rf usr/share/gtksourceview-3.0/styles /usr/share/gtksourceview-3.0/styles

echo "Configure X"
sudo -u $SUDO_USER cp -rf .Xresources $SUDO_HOME
sudo -u $SUDO_USER cp -rf .xsettingsd-dark $SUDO_HOME
sudo -u $SUDO_USER cp -rf .xsettingsd-light $SUDO_HOME

echo "Configure xdm"
cp -rf xdm /etc/X11
echo 'exec dwm' > $SUDO_HOME/.Xsession

# Scripts
cp -rf usr /
chmod ugo+x /usr/bin/my-audio-dl
chmod ugo+x /usr/bin/my-bookmarks
chmod ugo+x /usr/bin/my-bookmarks-music
chmod ugo+x /usr/bin/my-bookmarks-videos
chmod ugo+x /usr/bin/my-cloud-sync
chmod ugo+x /usr/bin/my-color-picker
chmod ugo+x /usr/bin/my-file-finder
chmod ugo+x /usr/bin/my-keyboard-layout
chmod ugo+x /usr/bin/my-logout
chmod ugo+x /usr/bin/my-monitors
chmod ugo+x /usr/bin/my-passwords
chmod ugo+x /usr/bin/my-paste-handler
chmod ugo+x /usr/bin/my-screenshot
chmod ugo+x /usr/bin/my-video-dl
chmod ugo+x /usr/bin/my-volume
chmod ugo+x /usr/bin/my-vpn

# Create default user directories
sudo -u $SUDO_USER xdg-user-dirs-update

# Cloud Share
apt install -y davfs2 rsync
# Add user to davfs2 group
usermod -a -G davfs2 $SUDO_USER
